/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const jwt = require('jsonwebtoken')
function jwtSignUser(user) {
  const eight_minutes = 60 * 8 
  return jwt.sign({data:user}, 'secret', {
    expiresIn: eight_minutes
  })
}

module.exports = {
  list: function (req, res) {
    User.find({}).exec(function (err, users) { //goes to the article model and fetch.
      if (err) {
        res.send(500, {
          error: 'DatabaseError'
        });
      }
      res.send(users)
    });
  },

  register (req, res) {
    User.create(
      req.body
    ).exec(function (err, user) {
      if (err) {
        res.send(500, {
          error: 'Error! Check Input Fields ' + err
        });
      }
      res.send({user: user});
    });
  },

  

  async login (req, res) {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({
        where: {
          username: username
        }
      })
      if (!user) {
        return res.send(403, {
          error: 'This login information was incorrect'
        })
      }
      const isPasswordValid = password === user.password
      if (!isPasswordValid) {
        res.status(403).send({
          error: 'This login information was incorrect-pass'
        })
      }
      res.send({user:user, token: jwtSignUser(user)})
      
    } catch (error) {
      res.send(500, {
        error: 'Error occured on Log ' + error
      });
    }


  },

};
