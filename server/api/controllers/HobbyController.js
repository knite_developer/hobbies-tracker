/**
 * HobbyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
// const jwt = require('jsonwebtoken')
// import { Twilio } from "twilio";
const client = require('twilio')('ACfa61a05d405ee7dee6952ce03083a4c4', 'b7afc9c9bc9fe34a0e589b9f3113f3e3')
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken')

function twilio(number, hobby) {
  client.messages.create({
    to: number,
    from: '+17014016187',
    body: `You have a message from the Hobby app. Your Hobby '${hobby}' has been added.`
  }, function (err, data) {
    if (err) {
      console.log(err)
    }
    console.log(data)
  })
}

function sendEmail(emailAddress, hobby) {
  nodemailer.createTestAccount((err, account) => {
    let transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: 'Your_email',
        pass: 'Your_password'
      },
      tls: {
        rejectUnauthorized: false
      }
    });
    

    let mailOptions = {
      from: '"HobbyApp 👻" <HobbyApp@test.com>',
      to: emailAddress,
      subject: 'Hello ✔, New Hobby Created',
      // text:  ,
      html: `Hello your hobby '${hobby}' has been created`
    };

    transporter.sendMail(mailOptions, (error, info) => {
      
      if (error) {
        return console.log(error);
      }
      console.log('Message sent: %s', info.messageId);

      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

    });
  });
}


module.exports = {

  addHobby(req, res) {
    jwt.verify(req.headers.authorizationtoken, 'secret', function (error, data) {
      if (error) {
        res.status(600).send({error: 'TokenExpires'})
      } else {
        const {
          userId,
          username,
          userEmail,
          hobby
        } = req.body;
        const phoneNumber = req.body.phoneNumber
        Hobby.create({
          userId: userId,
          username: username,
          userEmail: userEmail,
          hobby: hobby
        }).exec(function (err) {
          if (err) {
            res.status(500.).send({error:'DatabaseError ' + err});
          } else {
            // Send SMS
            twilio(phoneNumber, hobby)
            // Send Email
            sendEmail(userEmail, hobby)
            res.send()
          }
        });
      }
    })
  },

  viewAllHobby: (req, res) => {
    Hobby.find({}).exec(function (err, hobbies) {
      if (err) {
        res.status(500.).send({error:'DatabaseError ' + err});
      } else {
        res.send(hobbies)
      }
    })
  },


  viewHobbyByUser: (req, res) => {
    jwt.verify(req.headers.authorizationtoken, 'secret', function (error, data) {
      if (error) {
        console.log(error)
        res.status(600).send({error: 'TokenExpires'})
      } else {
        Hobby.find({
          userId: req.params.id
        }).exec(function (err, hobbies) {
          if (err) {
            res.status(500.).send({error:'DatabaseError ' + err});
          }
            res.send(hobbies)
        });
      }
    })
  },

  fetchEditHobby: function (req, res) {
    jwt.verify(req.headers.authorizationtoken, 'secret', function (error, data) {
      if (error) {
        res.status(600).send({error: 'TokenExpires'})
      } else {
        Hobby.findOne({
          id: req.params.id
        }).exec(function (err, hobby) { //goes to the hobby model and fetch.
          if (err) {
           res.status(500.).send({error:'DatabaseError ' + err});
          } else {
            res.send(hobby);
          }
        });
      }
    })

  },

  updateHobby: (req, res) => {
    jwt.verify(req.headers.authorizationtoken, 'secret', function (error, data) {
      if (error) {
        res.status(600).send({error: 'TokenExpires'})
      } else {
        Hobby.update({
          id: req.params.id,
        }, {
          hobby: req.body.editedHobby,
        }).exec(function (err) {
          if (err) {
            res.status(500.).send({error:'DatabaseError ' + err});
          } else {
            res.send()
          }
        });
        return false;
      }
    })
  },

  deleteHobby: (req, res) => {
    jwt.verify(req.headers.authorizationtoken, 'secret', function (error, data) {
      if (error) {
        res.status(600).send({error: 'TokenExpires'})
      } else {
        Hobby.destroy({
          id: req.params.id
        }).exec(function (err) {
          if (err) {
            res.status(500.).send({error:'DatabaseError ' + err});
          } else {
          res.send('Done');
          }
        });
        return false;
      }
    })
  }



};
