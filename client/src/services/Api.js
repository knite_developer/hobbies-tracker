import axios from 'axios'
import Store from '@/store/store'

export default () => {
  return axios.create({
    baseURL: `http://localhost:1337`,
    headers: {'authorizationToken': Store.state.token}
  })
}
