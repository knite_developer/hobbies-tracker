import Api from '@/services/Api'

export default {
  register (regCredentials) {
    return Api().post('user/register', regCredentials)
  },
  login (logCredentials) {
    return Api().post('user/login', logCredentials)
  }
}
