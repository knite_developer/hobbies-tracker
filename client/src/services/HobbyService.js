import Api from '@/services/Api'

export default {
  addHobby (credentials) {
    return Api().post('/hobby/addHobby', credentials)
  },
  viewAllHobby () {
    return Api().get(`/hobby/viewAllHobby`)
  },
  viewHobbyByUser (userId) {
    return Api().get(`/hobby/viewHobbyByUser/${userId}`)
  },
  updateHobby (editedHobby, hobbyId) {
    return Api().post(`/hobby/updateHobby/${hobbyId}`, editedHobby)
  },
  fetchEditHobby (hobbyId) {
    return Api().get(`/hobby/fetchEditHobby/${hobbyId}`)
  },
  deleteHobby (hobbyId) {
    return Api().post(`/hobby/deleteHobby/${hobbyId}`)
  }
}
