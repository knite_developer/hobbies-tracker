import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Register from '@/components/Register'
import AddHobby from '@/components/AddHobby'
import ViewHobby from '@/components/ViewHobby'
import EditHobby from '@/components/EditHobby'
import BrowseHobbies from '@/components/BrowseHobbies'
import SignIn from '@/components/SignIn'
import SignOut from '@/components/SignOut'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/signIn',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signOut',
      name: 'SignOut',
      component: SignOut
    },
    {
      path: '/hobbies/add',
      name: 'AddHobby',
      component: AddHobby
    },
    {
      path: '/browse',
      name: 'BrowseHobbies',
      component: BrowseHobbies
    },
    {
      path: '/hobbies/view',
      name: 'ViewHobby',
      component: ViewHobby
    },
    {
      path: '/hobbies/edit/:id',
      name: 'EditHobby',
      component: EditHobby
    }
  ]
})
